# AutoComplete

resource "aws_api_gateway_resource" "autocomplete" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.v1.id
  path_part   = "autocomplete"
}

# AutoComplete RCS

resource "aws_api_gateway_resource" "MyAutoComplete" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.autocomplete.id
  path_part   = "{terms}"
}
