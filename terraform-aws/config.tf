resource "aws_api_gateway_rest_api" "api_gateway" {
  name        = "[EcomAPP] App Home - Features"
  description = "API v1 (AWS) migrada a partir da sensedia v2"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "v1" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_rest_api.api_gateway.root_resource_id
  path_part   = "v1"
}
