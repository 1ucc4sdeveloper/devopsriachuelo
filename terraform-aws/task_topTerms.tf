# Top-terms

resource "aws_api_gateway_resource" "top-terms" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.v1.id
  path_part   = "top-terms"
}

