#Home

resource "aws_api_gateway_resource" "home" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.v1.id
  path_part   = "home"
}

# Home SCR

resource "aws_api_gateway_resource" "rcs" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.home.id
  path_part   = "{rcs}"
}

