# Find

resource "aws_api_gateway_resource" "find" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.v1.id
  path_part   = "find"
}

# Find - TERM

resource "aws_api_gateway_resource" "myFind" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.find.id
  path_part   = "{term}"
}
