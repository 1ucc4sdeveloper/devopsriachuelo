# Search

resource "aws_api_gateway_resource" "search" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.v1.id
  path_part   = "search"
}

resource "aws_api_gateway_resource" "mySearch" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.search.id
  path_part   = "{rcs}"
}
