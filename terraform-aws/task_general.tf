#General

resource "aws_api_gateway_resource" "general" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.v1.id
  path_part   = "general"
}

#General Countries

resource "aws_api_gateway_resource" "countries" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.general.id
  path_part   = "countries"
}

#General Date

resource "aws_api_gateway_resource" "date" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id   = aws_api_gateway_resource.general.id
  path_part   = "date"
}
